.. Date: September 09 2021
.. Author: kquinn

*****************************************
Overview
*****************************************
A high level overview of the steps to follow to install the NVIDIA GPU Operator are listed below:

#. :ref:`cluster-entitlement`.
#. :ref:`install-nfd`.
#. :ref:`install-nvidiagpu`.
#. :ref:`running-sample-app`.
